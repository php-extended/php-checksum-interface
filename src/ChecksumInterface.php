<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-checksum-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Checksum;

use Iterator;
use Stringable;

/**
 * ChecksumInterface interface file.
 * 
 * This interface specifies the functionnalities of the given checksum or
 * verification algorithm.
 * 
 * @author Anastaszor
 */
interface ChecksumInterface extends Stringable
{
	
	/**
	 * Calculates and returns the checksum for the given data.
	 * 
	 * @param ?string $data
	 * @return string
	 */
	public function calculate(?string $data) : string;
	
	/**
	 * Calculates and returns the checksum after iterating over the whole array.
	 * This can be used to stream file data through chunks of binary data of
	 * fixed size.
	 * 
	 * @param array<integer, ?string> $data
	 * @return string
	 */
	public function calculateOverAll(array $data) : string;
	
	/**
	 * Calculates and returns the checksum after iterating over the whole
	 * iterator. This can be used to stream file data through chunks of binary
	 * data of fixed size.
	 * 
	 * @param Iterator<integer, ?string> $iterator
	 * @return string
	 */
	public function calculateOverIterator(Iterator $iterator) : string;
	
	/**
	 * Returns an array where all the values of the output array are the
	 * checksums of the values from the input array. Keys are kept the same.
	 * 
	 * @param array<integer, ?string> $data
	 * @return array<integer, string>
	 */
	public function calculateAll(array $data) : array;
	
	/**
	 * Returns an iterator where all the values of the output iterator are the
	 * checksums of the values from the input iterator. Keys are kept the same.
	 * 
	 * @param Iterator<integer, ?string> $iterator
	 * @return Iterator<integer, string>
	 */
	public function calculateIterator(Iterator $iterator) : Iterator;
	
	/**
	 * Gets whether the given data matches the given checksum. If the provided 
	 * checksum does not matches the return format of this checksum algorithm,
	 * then the result is always false.
	 * 
	 * @param string $checksum
	 * @param ?string $data
	 * @return boolean
	 */
	public function matches(string $checksum, ?string $data) : bool;
	
	/**
	 * Gets whether the given data provided by the given array matches the
	 * given checksum. If the provided checksum does not matches the return
	 * format of this checksum algorithm, then the result is always false.
	 * 
	 * @param string $checksum
	 * @param array<integer, ?string> $data
	 * @return boolean
	 */
	public function matchesOverAll(string $checksum, array $data) : bool;
	
	/**
	 * Gets whether the given data provided by the given iterator matches the
	 * given checksum. If the provided checksum does not matches the return
	 * format of this checksum algorithm, then the result is always false.
	 * 
	 * @param string $checksum
	 * @param Iterator<integer, ?string> $iterator
	 * @return boolean
	 */
	public function matchesOverIterator(string $checksum, Iterator $iterator) : bool;
	
	/**
	 * Filters in all the matching data, meaning this method returns an array
	 * where only the data that matches are kept inside the output array.
	 * Each value inside the input array is evaluated individually.
	 * 
	 * Array keys are the data, array values are the checksum values to be
	 * matched against.
	 * 
	 * @param array<string, string> $data
	 * @return array<string, string>
	 */
	public function filterAllMatching(array $data) : array;
	
	/**
	 * Filters in all the matching data, meaning this method returns an Iterator
	 * where only the data that matches are kept inside the output Iterator.
	 * Each value inside the input Iterator is evaluated individually.
	 *
	 * Iterator keys are the data, Iterator values are the checksum values to be
	 * matched against.
	 *
	 * @param Iterator<string, string> $iterator
	 * @return Iterator<string, string>
	 */
	public function filterIteratorAllMatching(Iterator $iterator) : Iterator;
	
	/**
	 * Filters in all the matching data, meaning this method returns an array
	 * where only the data that do NOT match are kept inside the output array.
	 * Each value inside the input array is evaluated individually.
	 *
	 * Array keys are the data, array values are the checksum values to be
	 * matched against.
	 *
	 * @param array<string, string> $data
	 * @return array<string, string>
	 */
	public function filterNoneMatching(array $data) : array;
	
	/**
	 * Filters in all the matching data, meaning this method returns an Iterator
	 * where only the data that do NOT match are kept inside the output Iterator.
	 * Each value inside the input Iterator is evaluated individually.
	 *
	 * Iterator keys are the data, Iterator values are the checksum values to be
	 * matched against.
	 *
	 * @param Iterator<string, string> $iterator
	 * @return Iterator<string, string>
	 */
	public function filterIteratorNoneMatching(Iterator $iterator) : Iterator;
	
}
